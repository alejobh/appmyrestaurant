package com.example.alejandro.restaurante;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class RestauranteDbHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "RestauranteDB.db";
    public static final int DATABASE_VERSION = 5;
    public static final String TABLE_PRODUCTOS = "PRODUCTOS";
    public static final String TABLE_USERS = "USERS";

    public RestauranteDbHelper(Context context){
            super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        //Crear la tabla tipos productos
       // db.execSQL(RestauranteDataSource.CREATE_TIPOS_PRODUCTOS_SCRIPT);


        //Crear la tabla productos
        db.execSQL(RestauranteDataSource.CREATE_PRODUCTOS_SCRIPT);

        //Crear la tabla USERS
        db.execSQL(RestauranteDataSource.CREATE_USERS_SCRIPT);

        //Insertar registros iniciales
        //db.execSQL(RestauranteDataSource.INSERT_TIPOS_PRODUCTO_SCRIPT1);
        //db.execSQL(RestauranteDataSource.INSERT_TIPOS_PRODUCTO_SCRIPT2);
        //db.execSQL(RestauranteDataSource.INSERT_TIPOS_PRODUCTO_SCRIPT3);
        db.execSQL(RestauranteDataSource.INSERT_PRODUCTOS_SCRIPT);
        db.execSQL(RestauranteDataSource.INSERT_USERS_SCRIPT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }

}