package com.example.alejandro.restaurante;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;

public class EntradasView extends AppCompatActivity {
    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;
    private RestauranteDbHelper openHelper;
    private SQLiteDatabase database;
    public static final String RESTAURANTE_TABLE_PRODUCTOS = "PRODUCTOS";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entradas_view);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Nuestras entradas");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        List items = new ArrayList();
        openHelper = new RestauranteDbHelper(this);
        database = openHelper.getWritableDatabase();

        String query = "select * from " + RESTAURANTE_TABLE_PRODUCTOS + " WHERE TIPO=?";
        Cursor c =  database.rawQuery( "select * from PRODUCTOS where TIPO=\"Entrada\"", null );
        while(c.moveToNext()){
            String imageName = c.getString(4);
            int resID = getResources().getIdentifier(imageName, "drawable", getPackageName());
            items.add(new Producto(resID, c.getString(3), c.getString(2), "Valor: $"+c.getString(5)));
            //new producto (img, nombre, descripcion, precio)

        }

        // Obtener el Recycler
        recycler = (RecyclerView) findViewById(R.id.reciclador);
        recycler.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(lManager);

        // Crear un nuevo adaptador
        adapter = new ProductoAdapter(items);
        recycler.setAdapter(adapter);
    }
}
