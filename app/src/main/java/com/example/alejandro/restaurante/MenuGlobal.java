package com.example.alejandro.restaurante;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MenuGlobal extends AppCompatActivity {

    private ImageButton bebidas;
    private ImageButton entradas;
    private ImageButton platosFuertes;
    private RestauranteDataSource dataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_global);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Sección Carta");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        //Crear nuevo objeto RestauranteDataSource
        dataSource = new RestauranteDataSource(this);

        entradas = (ImageButton)findViewById(R.id.btnEntradas);

        entradas.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent i = new Intent(getApplicationContext(), EntradasView.class);
                        startActivity(i);
                    }
                }
        );

        platosFuertes = (ImageButton)findViewById(R.id.btnPlatosFuertes);

        platosFuertes.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent i = new Intent(getApplicationContext(), PlatosFuertesView.class);
                        startActivity(i);
                    }
                }
        );

        bebidas = (ImageButton)findViewById(R.id.btnBebidas);

        bebidas.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent i = new Intent(getApplicationContext(), BebidasView.class);
                        startActivity(i);
                    }
                }
        );

    }
}
