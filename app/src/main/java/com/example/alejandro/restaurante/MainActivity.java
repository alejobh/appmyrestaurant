package com.example.alejandro.restaurante;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {

    private Button user;
    private Button guest;
    public static String tipeUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Welcome to Spagetty");
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        user = (Button)findViewById(R.id.btnUsuario);
        guest = (Button)findViewById(R.id.btnInvitado);

        user.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        tipeUser = "registered";
                        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(i);
                    }
                }
        );

        guest.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        tipeUser = "guest";
                        Intent i = new Intent (MainActivity.this, MenuPrincipal.class);
                        startActivity(i);
                    }
                }

        );




    }
}
