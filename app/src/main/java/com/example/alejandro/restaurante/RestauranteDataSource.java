package com.example.alejandro.restaurante;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class RestauranteDataSource {

    //Metainformación de la base de datos
    public static final String RESTAURANTE_TABLE_PRODUCTOS = "PRODUCTOS";
    public static final String RESTAURANTE_TABLE_USERS = "USERS";
    public static final String STRING_TYPE = "text";
    public static final String INT_TYPE = "integer";

    //Campos de la tabla PRODUCTOS
    public static class ColumnProductos{
        public static final String ID_PRODUCTO = BaseColumns._ID;
        public static final String TIPO = "TIPO";
        public static final String DESCRIPCION = "DESCRIPCION";
        public static final String NAME_PRODUCTO = "NAME_PRODUCTO";
        public static final String PRECIO = "PRECIO";
        public static final String IMAGEN = "IMAGEN";
    }

        //Campos de la tabla USERS
        public static class ColumnUsers{
                public static final String ID_USER = BaseColumns._ID;
                public static final String USERNAME = "USERNAME";
                public static final String PASSW = "PASSW";
                public static final String MAIL = "MAIL";

        }

    //Script de Creación de la tabla PRODUCTOS
    public static final String CREATE_PRODUCTOS_SCRIPT =
            "CREATE TABLE "+ RESTAURANTE_TABLE_PRODUCTOS +"(" +
                    ColumnProductos.ID_PRODUCTO+" "+INT_TYPE+" primary key autoincrement," +
                    ColumnProductos.TIPO+" "+STRING_TYPE+" not null," +
                    ColumnProductos.DESCRIPCION+" "+STRING_TYPE+" not null," +
                    ColumnProductos.NAME_PRODUCTO+" "+STRING_TYPE+" not null," +
                    ColumnProductos.IMAGEN+" "+STRING_TYPE+" not null," +
                    ColumnProductos.PRECIO+" "+STRING_TYPE+" not null,"+
                    "UNIQUE("+ColumnProductos.ID_PRODUCTO+",TIPO,DESCRIPCION,NAME_PRODUCTO,IMAGEN,PRECIO))";

        public static final String CREATE_USERS_SCRIPT =
                "CREATE TABLE "+ RESTAURANTE_TABLE_USERS +"(" +
                        ColumnUsers.ID_USER+" "+INT_TYPE+" primary key autoincrement," +
                        ColumnUsers.USERNAME+" "+STRING_TYPE+" not null," +
                        ColumnUsers.PASSW+" "+STRING_TYPE+" not null," +
                        ColumnUsers.MAIL+" "+STRING_TYPE+" not null," +
                        "UNIQUE("+ColumnUsers.ID_USER+",USERNAME,PASSW,MAIL))";

    public static final String INSERT_PRODUCTOS_SCRIPT =
            "insert into "+ RESTAURANTE_TABLE_PRODUCTOS +" values(" +
                    "null," +
                    "\"Bebida\"," +
                    "\"refill incluido\"," +
                    "\"Bebida Gaseosa\","+
                    "\"gaseosa\","+
                    "\"4.000\")," +
                    "(null," +
                    "\"Bebida\"," +
                    "\"Limonada natural\"," +
                    "\"Limonada\","+
                    "\"limonada\","+
                    "\"4.500\")," +
                    "(null," +
                    "\"Bebida\"," +
                    "\"Limonada natural\"," +
                    "\"Limonada de coco\","+
                    "\"limonadac\","+
                    "\"4.500\")," +
                    "(null," +
                    "\"Bebida\"," +
                    "\"Cerveza\"," +
                    "\"Pilsen\","+
                    "\"pilsen\","+
                    "\"4.000\")," +
                    "(null," +
                    "\"Bebida\"," +
                    "\"Cerveza\"," +
                    "\"Aguila\","+
                    "\"aguila\","+
                    "\"4.000\")," +
                    "(null," +
                    "\"Bebida\"," +
                    "\"Club Colombia Negra\"," +
                    "\"Club Colombia\","+
                    "\"ccnegra\","+
                    "\"4.500\")," +
                    "(null," +
                    "\"Bebida\"," +
                    "\"Club Colombia Dorada\"," +
                    "\"Club Colombia\","+
                    "\"ccdorada\","+
                    "\"4.500\")," +
                    "(null," +
                    "\"Entrada\"," +
                    "\"La tradicional sopa francesa de sabor intenso, con pan francés y queso mozarella\"," +
                    "\"Sopa de cebolla\","+
                    "\"sopacebolla\","+
                    "\"8.800\")," +
                    "(null," +
                    "\"Entrada\"," +
                    "\"Tomate, albahaca, queso parmesano y crema de leche\"," +
                    "\"Sopa de tomate\","+
                    "\"sopatomate\","+
                    "\"8.800\")," +
                    "(null," +
                    "\"Entrada\"," +
                    "\"champiñones cocidos y salteados en vino blanco con crema de leche y parmesano\"," +
                    "\"Crema de champiñones\","+
                    "\"cremahongo\","+
                    "\"9.400\")," +
                    "(null," +
                    "\"Entrada\"," +
                    "\"Salidos del horno con mantequilla de hierbas y ajo\"," +
                    "\"Pancitos\","+
                    "\"pancitos\","+
                    "\"3.300\")," +
                    "(null," +
                    "\"Entrada\"," +
                    "\"Solomito en finos trozos con toque de tocineta, tomates rojos y albahaca\"," +
                    "\"Patata Romana\","+
                    "\"patata\","+
                    "\"10.900\")," +
                    "(null," +
                    "\"PlatoFuerte\"," +
                    "\"La carne cocida a fuego muy lento se funde con tomates maduros y algunas hierbas que le dan su toque italiano\"," +
                    "\"Pasta Boloñesa\","+
                    "\"bolonesa\","+
                    "\"16.900\")," +
                    "(null," +
                    "\"PlatoFuerte\"," +
                    "\"Clasica salsa italiana, parmesano. tocineta, champiñones portobello\"," +
                    "\"Pasta Carbonara\","+
                    "\"carbonara\","+
                    "\"17.500\")," +
                    "(null," +
                    "\"PlatoFuerte\"," +
                    "\"Carne, pollo y champiñones queso mozarella de bufala y parmesano\"," +
                    "\"Lasaña carne, pollo y champiñones\","+
                    "\"lasana\","+
                    "\"10.900\")," +
                    "(null," +
                    "\"PlatoFuerte\"," +
                    "\"Con salsa napolitana, napolitana con queso , salsa rosa o de champiñones\"," +
                    "\"Raviolis de pollo y jamón ahumado\","+
                    "\"raviolis\","+
                    "\"16.900\");";

        public static final String INSERT_USERS_SCRIPT =
                "insert into "+ RESTAURANTE_TABLE_USERS +" values(" +
                        "null," +
                        "\"oscarsanchez\"," +
                        "\"asd123\"," +
                        "\"oscar.sanchez@upb.edu.co\"),"+
                        "(null," +
                        "\"alejobh\"," +
                        "\"asd123\"," +
                        "\"alejandro.bermudez@upb.edu.co\"),"+
                        "(null," +
                        "\"firal\"," +
                        "\"asd123\"," +
                        "\"danielf.giraldo@upb.edu.co\");";

    private RestauranteDbHelper openHelper;
    private SQLiteDatabase database;

        public Cursor getAllBebidas(){
                //Seleccionamos todas las filas de la tabla Quotes
                String query = "select * from " + RESTAURANTE_TABLE_PRODUCTOS + "WHERE TIPO=?";

                return database.rawQuery(query, new String[]{"\"Bebida\""});
        }

    public RestauranteDataSource(Context context) {
        //Creando una instancia hacia la base de datos
        openHelper = new RestauranteDbHelper(context);
        database = openHelper.getWritableDatabase();
    }



}