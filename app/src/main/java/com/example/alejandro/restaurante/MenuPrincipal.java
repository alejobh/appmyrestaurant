package com.example.alejandro.restaurante;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MenuPrincipal extends AppCompatActivity {

    private ImageButton verCarta;
    private ImageButton verSedes;
    private ImageButton btnReserva;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Menu Principal");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btnReserva = (ImageButton)findViewById(R.id.btnReserva);

        verCarta = (ImageButton)findViewById(R.id.btnVerCarta);

        verCarta.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent i = new Intent (MenuPrincipal.this, MenuGlobal.class);
                        startActivity(i);
                    }
                }
        );

        verSedes = (ImageButton)findViewById(R.id.btnSedes);

        verSedes.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
                        Intent i = new Intent (MenuPrincipal.this, MapsActivity.class);
                        startActivity(i);
                    }
                }
        );

        if(MainActivity.tipeUser.equals("guest"))
        {
            btnReserva.setVisibility(View.GONE);
        }
        else if(MainActivity.tipeUser.equals("registered")){
            btnReserva.setVisibility(View.VISIBLE);
        }

    }
}
