package com.example.alejandro.restaurante;


public class Producto {

    private int imagen;
    private String nombre;
    private String descripcion;
    private String costo;

    public Producto(int imagen, String nombre, String descripcion, String costo) {
        this.imagen = imagen;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.costo = costo;
    }

    public int getImagen() {
        return imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getCosto() {
        return costo;
    }
}
